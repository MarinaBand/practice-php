const radioEl =  document.querySelector('.methods-container');
const vietaOptionsEl = document.querySelector('.vieta-options');

radioEl.addEventListener(
    'click',
    (e) => {
        if (!e.target.classList.contains('method')) {
            return;
        }
        switch (e.target.getAttribute('id')) {
            case 'vieta':
                vietaOptionsEl.removeAttribute('hidden');
                break;
            case 'discriminant':
                if (!vietaOptionsEl.hasAttribute('hidden')) {
                    vietaOptionsEl.setAttribute('hidden', 'hidden');
                }
                break;
        }
    }
);