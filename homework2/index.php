<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <title>Document</title>
</head>
<body>
<?php
include ('menu.php');
function showLinks ($parent = 0) {
    global $menu;
    $result = "";
    foreach ($menu as $index => $item) {
        if ($item['parent'] == $parent) {
            if ($parent == 0) {
                $id = $item['id'];
                $pointer = array_filter($menu, fn($a) => $a['parent'] == $id) ? " dropdown-toggle" : "" ;
                $dropdown = array_filter($menu, fn($a) => $a['parent'] == $id) ? " data-toggle='dropdown'" : "" ;
                $aAttributeList = "class='nav-link$pointer'$dropdown aria-haspopup='true' aria-expanded='false'";
                $uAttributeList = "class='navbar-nav mr-auto'";
            } else {
                $aAttributeList = "class='dropdown-item'";
                $uAttributeList = "class='dropdown-menu bg-light' aria-labelledby='navbarDropdown'";
            }
            $externalLinkIcon = $item['out'] == 1 ? "<i class='fas fa-external-link-alt'></i>" : "" ;
            $result .= "<li class='nav-item active dropdown' title='$item[title]'><a href='#/$item[url]' $aAttributeList>$item[name]</a>$externalLinkIcon" . showLinks($item['id']) . "</li>";
            unset($menu[$index]);

        }
    }
    return $result !== "" ? "<ul $uAttributeList>$result</ul>" : $result;
}
function showMenu () {
    $result = showLinks();
    return $result !== "" ? "<nav class='navbar navbar-expand-lg navbar-light bg-light'>
   <div class='collapse navbar-collapse' id='navbarSupportedContent'>$result</div></nav>" : $result;
}
echo showMenu();
?>
<script src="https://kit.fontawesome.com/156bff3523.js" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
