<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
if (isset($_POST['create-table'])) {
    $tempFile = './tempList.php';
    if (!file_exists($tempFile)) {
        $file = copy('./list.php', './tempList.php');
        $template = ');';
        $file = fopen($tempFile , 'a');
        fputs($file, $template);
        fclose($file);
    }
    include('tempList.php');
    usort($list, function ($a, $b){
        return $a['surname'] <=> $b['surname'];
    });
    echo showTable($list);
    unlink('tempList.php');
}

function showTable($arr) {
    $result = "<table border='1'>
                    <thead>
                        <tr>
                            <th>Фамилия</th>
                            <th>Имя</th>
                            <th>Отчество</th>
                            <th>Дата рождения</th>
                            <th>Спортивный разряд</th>
                            <th>Медицинская справка</th>
                            <th>Разрядная книжка</th>
                        </tr>
                    </thead>
                    <tbody>";
    foreach ($arr as $value) {
        $result .= "
                <tr>
                    <td>$value[surname]</td>
                    <td>$value[name]</td>
                    <td>$value[patronymic]</td>
                    <td>$value[birthday]</td>
                    <td>$value[athleticTitle]</td>
                    <td><a href='$value[medicalUrl]' target='_blank'><img src='$value[medicalUrl]' width='200px' height='150px'></a></td>
                    <td><a href='$value[certificateUrl]' target='_blank'><img src='$value[certificateUrl]' width='200px' height='150px'></a</td>
                </tr>";
    }
    $result .= "</tbody></table>";
    return $result;
}
?>
</body>
</html>
