document.querySelector("#postForm").addEventListener('submit', newPost);

function newPost(e) {
    e.preventDefault();

    const nameInput = document.querySelector("#name");
    const imgInput = document.querySelector("#inputGroupFile01");
    const response = document.querySelector(".response");
    if (!nameInput.value) {
        response.innerHTML = "<p style='color: red'>Введите имя пользователя</p>";
        return;
    }
    if (!imgInput.value) {
        response.innerHTML = "<p style='color: red'>Выберите фотографии для зарузки</p>";
        return;
    }
    if (imgInput.files.length > 15) {
        response.innerHTML = "<p style='color: red'>Превышено максимальное количество загружаемых файлов - 15</p>";
        return;
    }
    const request = new XMLHttpRequest();
    const url = 'server.php';
    const postAlbum = new FormData();
    postAlbum.append('name', nameInput.value);
    request.open('POST', url);
    request.send(postAlbum);

    const progressBar = document.querySelector(".progress");
    progressBar.removeAttribute('hidden');
    let i = 0;
    setTimeout(
        function delay() {
            let progress = Math.round((i + 1) / imgInput.files.length * 100);
            progressBar.innerHTML = `<div class="progress-bar progress-bar-striped" role="progressbar" style="width: ${progress}%" aria-valuenow="${progress}" aria-valuemin="0" aria-valuemax="100"></div>`;
            postAlbum.append(i, imgInput.files[i], imgInput.files[i].name);
            request.open("POST", url);
            request.onreadystatechange = function () {
                if (request.readyState === 4 && request.status === 200) {
                    response.innerHTML = this.responseText;
                }
            }
            request.send(postAlbum);
            i++;
            if (i < imgInput.files.length) {
                setTimeout(delay, 1000);
            } else {
                progressBar.setAttribute('hidden', 'hidden');
                nameInput.value = '';
                imgInput.value = '';
            }
        },
        1000
    );
}