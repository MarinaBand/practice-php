<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Решение квадратных уравнений</title>
</head>
<body>
    <form method="get">
        A:<input type="text" name="a"><br>
        B:<input type="text" name="b"><br>
        C:<input type="text" name="c"><br>
        <div class="methods-container">
            Способ решения:<br>
            <div>
                <input type="radio" name="method" class="method" id="discriminant" value="discriminant" checked>
                <label for="discriminant">Дискриминант</label>
            </div>
            <div>
                <input type="radio" name="method" class="method" id="vieta" value="vieta">
                <label for="vieta">Теорема Виета</label>
            </div>
        </div>
        <div class="vieta-options" hidden>
            Выберите диапазон поиска корней:<br>
            С:<input type="text" name="from"><br>
            По:<input type="text" name="to"><br>
        </div>
        <input type="submit" value="Решить уравнение">
    </form>
    <?php
        if (isset($_GET['a'])) {
            $a = htmlentities($_GET['a']);
            $b = htmlentities($_GET['b']);
            $c = htmlentities($_GET['c']);
            $method = htmlentities($_GET['method']);
            $from = htmlentities($_GET['from']);
            $to = htmlentities($_GET['to']);
            if (is_numeric($a) && is_numeric($b) && is_numeric($c)) {
                switch ($method) {
                    case 'discriminant':
                        $d = $b ** 2 - 4 * $a * $c;
                        //$d = round($b ** 2, 4) - 4 * round($a * $c, 4);
                        echo $d;
                        if ($d < 0) {
                            echo 'Нет корней';
                        } elseif ($d == 0) {
                            $x1 = round((-$b + $d ** 0.5) / (2 * $a), 2);
                            echo "x1 = x2 = " . $x1;
                        } else {
                            $x1 = round((-$b + $d ** 0.5) / (2 * $a), 2);
                            $x2 = round((-$b - $d ** 0.5) / (2 * $a), 2);
                            echo "x1 = " . $x1 . ", x2 = " . $x2;
                        }
                        break;
                    case 'vieta':
                        if ($a == 1) {
                            $p = $b;
                            $q = $c;
                        } else {
                            $p = $b / $a;
                            $q = $c / $a;
                        }
                        if (is_numeric($from) && is_numeric($to) && $from <= $to){
                            $x1 = '';
                            $x2 = '';
                            for ($i = $from; $i <= $to; $i = round($i + 0.01, 2)){
                                for ($j = $from; $j <= $to; $j = round($j + 0.01, 2)){
                                    if ($i + $j == -$p && $i * $j == $q) {
                                        $x1 = $i;
                                        $x2 = $j;
                                        break 2;
                                    }
                                }
                            }
                            echo (is_numeric($x1)) ? "x1 = $x1, x2 = $x2" : "Нет корней";
                        } else {
                            echo "Введите корректные значения диапазона поиска корней";
                        }
                        break;
                    default:
                        echo "Выберите способ решения";
                }
            } else {
                echo "Введите корректные параметры квадратного уравнения";
            }
        }
    ?>
    <script>
        const radioEl =  document.querySelector('.methods-container');
        const vietaOptionsEl = document.querySelector('.vieta-options');

        radioEl.addEventListener(
            'click',
            (e) => {
                if (!e.target.classList.contains('method')) {
                    return;
                }
                switch (e.target.getAttribute('id')) {
                    case 'vieta':
                        vietaOptionsEl.removeAttribute('hidden');
                        break;
                    case 'discriminant':
                        if (!vietaOptionsEl.hasAttribute('hidden')) {
                            vietaOptionsEl.setAttribute('hidden', 'hidden');
                        }
                        break;
                }
            }
        );
    </script>
</body>
</html>