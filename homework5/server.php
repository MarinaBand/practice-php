<?php

if (isset($_POST['name'])) {
    $name = htmlentities($_POST['name']);
    $directory = "./img/$name";
    if (isset($_FILES['0'])) {
        $img = $_FILES;
        for ($i = 0; $i < count($_FILES); $i++) {
            if ($img[$i]['size'] >= 10000000) {
                echo "<p style='color: red'>Превышен максимальный размер файла изображения - 10 мб</p>";
            } elseif ($img[$i]['type'] !== 'image/gif' && $img[$i]['type'] !== 'image/pjpeg' && $img[$i]['type'] !== 'image/jpeg' && $img[$i]['type'] !== 'image/png') {
                echo "<p style='color: red'>Некорректный формат файла изображения</p>";
            } else {
                if (!is_dir($directory)) {
                    mkdir($directory, 0777);
                }
                $fileName = $img[$i]['name'];
                move_uploaded_file($img[$i]["tmp_name"], "$directory/$fileName");
            }
        }
    }
}