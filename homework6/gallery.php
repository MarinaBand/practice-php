<?php
$title = 'Галерея';

require_once 'templates/common/head.html';
require_once 'templates/common/header.html';

require_once 'templates/pages/gallery.html';

require_once 'templates/common/footer.html';
require_once 'templates/common/scriptside.html';