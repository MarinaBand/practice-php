<?php
$title = 'Наши цели';

require_once 'templates/common/head.html';
require_once 'templates/common/header.html';

require_once 'templates/pages/goals.html';

require_once 'templates/common/footer.html';
require_once 'templates/common/scriptside.html';