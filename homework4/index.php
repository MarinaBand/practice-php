<?php
require_once 'form.html';
if (isset($_POST['surname'])) {
    $surname = htmlentities($_POST['surname']);
    $name = htmlentities($_POST['name']);
    $patronymic = htmlentities($_POST['patronymic']);
    $birthday = htmlentities($_POST['birthday']);
    $athleticTitle = htmlentities($_POST['athletic-title']);
    if (isset($_FILES["medical-clearance"])) {
        $medicalFile = $_FILES["medical-clearance"];
        $certificateFile = $_FILES["athletic-certificate"];
        $medicalFileName = $medicalFile["name"];
        $certificateFileName = $certificateFile["name"];
        if ($medicalFile['size'] >= 5000000 || $certificateFile['size'] >= 5000000) {
            echo "<p style='color: red'>Превышен максимальный размер файла изображения</p>";
        }
        elseif (($medicalFile['type'] !== 'image/gif' && $medicalFile['type'] !== 'image/pjpeg' && $medicalFile['type'] !== 'image/jpeg' && $medicalFile['type'] !== 'image/png') || ($certificateFile['type'] !== 'image/gif' && $certificateFile['type'] !== 'image/pjpeg' && $certificateFile['type'] !== 'image/jpeg' && $certificateFile['type'] !== 'image/png')) {
            echo "<p style='color: red'>Некорректный формат файла изображения</p>";
//        trigger_error("Некорректный формат файла изображения", E_USER_ERROR);
        }
        else {
            $newMedicalFileName = random_int(1000, 9999) . $surname . "_med_clearance." . nameSuffix($medicalFileName);
            $newCertificateFileName = random_int(1000, 9999) . $surname . "_athletic_certificate." . nameSuffix($medicalFileName);
            move_uploaded_file($medicalFile["tmp_name"], "files/$newMedicalFileName");
            move_uploaded_file($certificateFile["tmp_name"], "files/$newCertificateFileName");
            $listFile = './list.php';
            if (!file_exists($listFile)) {
                $file = fopen($listFile, 'w');
                $template = '<?php
    $list = array(';
                fwrite($file, $template);
                fclose($file);
            }
            $template = "
        array('surname' => '$surname', 'name' => '$name', 'patronymic' => '$patronymic', 'birthday' => '$birthday', 'athleticTitle' => '$athleticTitle', 'medicalUrl' => 'files/$newMedicalFileName', 'certificateUrl' => 'files/$newCertificateFileName'), ";
            $file = fopen($listFile , 'a');
            fputs($file, $template);
            fclose($file);
        }

    }

}
function nameSuffix($str) {
    $arr = preg_split("/([.]+)/", $str, -1);
    return end($arr);
}



