<?php
    require_once 'form.html';
if (isset($_POST['login'])) {
    $login = htmlentities($_POST['login']);
    $password = htmlentities($_POST['password']);
    $passwordConfirm = htmlentities($_POST['password-confirm']);
    $surname = htmlentities($_POST['surname']);
    $name = htmlentities($_POST['name']);
    $patronymic = htmlentities($_POST['patronymic']);
    $birthday = htmlentities($_POST['birthday']);
    $phoneNum = htmlentities($_POST['phone-num']);
    $email = htmlentities($_POST['email']);
    $eventName = htmlentities($_POST['event-name']);
    $eventDate = htmlentities($_POST['event-date']);
    $eventPlace = htmlentities($_POST['event-place']);
    $eventRating = htmlentities($_POST['event-rating']);
    $review = htmlentities($_POST['review']);
    if (!preg_match('/^[0-9a-z_\-]{8,30}$/i', $login)) {
        echo 'Login некорректен<br>';
    }
    if (!preg_match('/^[0-9a-z_\-\+\#\$]{8,30}$/i', $password)) {
        echo 'Password некорректен<br>';
    } elseif (!preg_match('/[0-9]+/', $password)) {
        echo 'Password должен содержать как минимум одну цифру<br>';
    } elseif (!preg_match('/[a-z]+/', $password)) {
        echo 'Password должен содержать как минимум одну строчную букву<br>';
    } elseif (!preg_match('/[A-Z]+/', $password)) {
        echo 'Password должен содержать как минимум одну прописную букву<br>';
    } elseif (!preg_match('/[_\-\+\#\$]+/', $password)) {
        echo 'Password должен содержать как минимум один спецсимвол<br>';
    }
    if ($password !== $passwordConfirm) {
        echo 'Password не сопадает<br>';
    }
    if (!preg_match('/^[0-9a-zа-я]+$/iu', $name)) {
        echo 'Имя введено некорректно<br>';
    }
    if (!preg_match('/^[0-9a-zа-я]*$/iu', $patronymic)) {
        echo 'Отчество введено некорректно<br>';
    }
    if (!preg_match('/^[0-9a-zа-я]+$/iu', $surname)) {
        echo 'Фамилия введена некорректно<br>';
    }
    if (!preg_match('/(\d{2})\.(\d{2})\.(\d{4})/', $birthday)) {
        echo 'Дата рождения введена некорректно<br>';
    } else {
        $date = preg_split('/([.]+)/', $birthday, -1);
        if (!checkdate($date[1],$date[0],$date[2])) {
            echo 'Дата рождения введена некорректно<br>';
        }
    }
    if (!preg_match('/^\+7\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}$/', $phoneNum)) {
        echo 'Телефон введен некорректно<br>';
    }
    if (!preg_match('/^[0-9a-z]([0-9a-z_\-])*@([0-9a-zа-я_\-])+\.(?2){2,10}$/iu', $email)) {
        echo 'Email введен некорректно<br>';
    }
    if (!preg_match('/^[0-9a-zа-я\-\s\'\"]+$/iu', $eventName)) {
        echo 'Мероприятие введено некорректно<br>';
    }
    if (!preg_match('/(\d{2})\.(\d{2})\.(\d{4})/', $eventDate)) {
        echo 'Дата мероприятия введена некорректно<br>';
    } else {
        $date = preg_split('/([.]+)/', $eventDate, -1);
        if (!checkdate($date[1],$date[0],$date[2])) {
            echo 'Дата мероприятия введена некорректно<br>';
        }
    }
    if (!preg_match('/^[0-9a-zа-я\-\s\'\"]+$/iu', $eventPlace)) {
        echo 'Место введено некорректно<br>';
    }
    if (!preg_match('/[1-5]/', $eventRating)) {
        echo 'Оценка некорректна<br>';
    }
    if (!preg_match('/(.){0,255}/iu', $review)) {
        echo 'Слишком длинный отзыв<br>';
    }
}
?>