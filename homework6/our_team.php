<?php
$title = 'Наша команда';
require 'team_data.php';

require_once 'templates/common/head.html';
require_once 'templates/common/header.html';

require_once 'templates/pages/our_team.html';

require_once 'templates/common/footer.html';
require_once 'templates/common/scriptside.html';